///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:               SCTP_Daemon_Dynamic_IPL4_CtrlFuncDef.cc
//  Rev:                <RnXnn>
//  Prodnr:             CNL 113 630
//  Updated:            2010-11-24
//  Contact:            http://ttcn.ericsson.se
//  Reference:          


#include "IPL4asp_PortType.hh"
#include "IPL4asp_PT.hh"
#include "SCTP_Daemon_Dynamic_Interface_Definitions.hh"

namespace SCTP__Daemon__Dynamic__IPL4__CtrlFunct {

  IPL4asp__Types::Result f__IPL4__listen(
    SCTP__Daemon__Dynamic__Interface__Definitions::SDD__Interface__PT& portRef,
    const IPL4asp__Types::HostName& locName,
    const IPL4asp__Types::PortNumber& locPort,
    const IPL4asp__Types::ProtoTuple& proto,
    const IPL4asp__Types::OptionList& options)
  {
    return f__IPL4__PROVIDER__listen(portRef, locName, locPort, proto, options);
  }
  
  IPL4asp__Types::Result f__IPL4__connect(
    SCTP__Daemon__Dynamic__Interface__Definitions::SDD__Interface__PT& portRef,
    const IPL4asp__Types::HostName& remName,
    const IPL4asp__Types::PortNumber& remPort,
    const IPL4asp__Types::HostName& locName,
    const IPL4asp__Types::PortNumber& locPort,
    const IPL4asp__Types::ConnectionId& connId,
    const IPL4asp__Types::ProtoTuple& proto,
    const IPL4asp__Types::OptionList& options)
  {
    return f__IPL4__PROVIDER__connect(portRef, remName, remPort,
                                      locName, locPort, connId, proto, options);
  }

  IPL4asp__Types::Result f__IPL4__close(
    SCTP__Daemon__Dynamic__Interface__Definitions::SDD__Interface__PT& portRef, 
    const IPL4asp__Types::ConnectionId& connId, 
    const IPL4asp__Types::ProtoTuple& proto)
  {
      return f__IPL4__PROVIDER__close(portRef, connId, proto);
  }

  IPL4asp__Types::Result f__IPL4__setUserData(
    SCTP__Daemon__Dynamic__Interface__Definitions::SDD__Interface__PT& portRef,
    const IPL4asp__Types::ConnectionId& connId,
    const IPL4asp__Types::UserData& userData)
  {
    return f__IPL4__PROVIDER__setUserData(portRef, connId, userData);
  }
  
  IPL4asp__Types::Result f__IPL4__getUserData(
    SCTP__Daemon__Dynamic__Interface__Definitions::SDD__Interface__PT& portRef,
    const IPL4asp__Types::ConnectionId& connId,
    IPL4asp__Types::UserData& userData)
  {
    return f__IPL4__PROVIDER__getUserData(portRef, connId, userData);
  }

  void f__IPL4__setGetMsgLen(
    SCTP__Daemon__Dynamic__Interface__Definitions::SDD__Interface__PT& portRef,
    const IPL4asp__Types::ConnectionId& connId,
    IPL4asp__Types::f__getMsgLen& f,
    const IPL4asp__Types::ro__integer& msgLenArgs)
  {
    f__IPL4__PROVIDER__setGetMsgLen(portRef, connId, f, msgLenArgs);
  }
  
} // namespace IPL4__user__CtrlFunct

