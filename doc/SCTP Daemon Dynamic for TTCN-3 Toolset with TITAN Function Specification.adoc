---
Author: Tímea Moder
Version: 155 17-CNL 113 630, Rev. A
Date: 2012-05-14
---
= SCTP Daemon Dynamic for TTCN-3 Toolset with TITAN, Function Specification
:author: Tímea Moder
:revnumber: 155 17-CNL 113 630, Rev. A
:revdate: 2012-05-14
:toc:

= Introduction

== How to Read this Document

This is the Function Specification for the SCTP Daemon Dynamic (SDD). SDD is developed for the TTCN-3 Toolset with TITAN. This document should be read together with Product Revision Information <<_2, [2]>>.

== Scope

The purpose of this document is to specify the functionality of the SCTP Daemon Dynamic application.

== References

[1] [[_1]]ETSI ES 201 873-1 v.3.1.1 (2005-06) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[2] [[_2]]109 21-CNL 113 630-1 Uen +
SCTP Daemon Dynamic for TTCN-3 Toolset with TITAN, Product Revision Information

[3] [[_3]]RFC 1006 +
ISO transport services on top of the TCP: Version 3

[4] [[_4]]198 17-CNL 113 630 Uen +
SCTP Daemon Dynamic for TTCN-3 Toolset with TITAN, User’s Guide

[5] [[_5]]155 17-CNL 113 531 Uen +
IPL4 Test Port for TTCN-3 Toolset with TITAN, Function Specification

[6] [[_6]]1/198 17-CRL 113 200 Uen +
TITAN User’s Guide

[7] [[_7]]198 17-CNL 113 531 Uen +
IPL4 Test Port for TTCN-3 Toolset with TITAN, User’s Guide

[8] [[_8]]6/198 17-CRL 113 200 Uen +
PROGRAMMER'S GUIDE API Technical Reference for TITAN TTCN-3 Test Executor

== Abbreviations

PPID:: Payload Protocol Identifier
RTE:: Run-Time Environment
SCTP:: Stream Control Transmission Protocol
SDD:: SCTP Daemon Dynamic
SUT:: System Under Test
TCP:: Transmission Control Protocol
TPKT:: Transport Packet
TTCN-3:: 	Testing and Test Control Notation version 3

== Terminology

SDD:: SCTP Daemon Dynamic, the abbreviation of this product.

= General

SDD provides the following features to a testcase:

* Defining server and client associations
* SCTP payload relaying from the SUT via TCP connections between SDD and the testcases
* Keeping connected associations alive between testcases
* Reconnect disconnected client associations
* Dynamically create new SCTP client and server associations from the testcase
* Close client and server associations from the testcase
* Subscribe to association events, if the associations are already present
* API to allow automatic handling of incoming messages (i.e. heartbeat messages) and events
* Function callbacks that allow the user to extend the daemon so that routing can be implemented based on upper layer protocols
* Automatic shutdown after an inactivity period

The usage of these features can be found in <<_4, [4]>>.

= Functional specification

The outline of the SDD’s architecture can be seen in <<figure-1, Figure 1>>. The SDD is a standalone application, which is executed independently of the TTCN-3 testcases. This results that the SCTP associations remain active between the executions of testcases.

When the SDD is started, it connects the configured client SCTP associations to the SUT, opens the configured server SCTP associations, and starts listening on a TCP port for incoming connection requests from the users (testcases). After the daemon accepted a TCP connection request, the SCTP messages sent by the testcase are forwarded to the selected SCTP association.

Naturally, the answers coming from the SUT are relayed back to the user as well. Thus, SDD provides an interface to access SCTP communication ports via TCP connections.
The daemon tries to re-establish client SCTP associations that were interrupted and the testcases are notified if the corresponding SCTP association is lost or becomes active again.

[[figure-1]]
image::images/architecture.png[title="SCTP Daemon Dynamic architecture"]

== TCP Interface

The messages between SDD and the daemon users are sent over one or more TCP connections, which are handled by IPL4 test port <<_7, [7]>>. The SDD source code contains a dual-faced (see for ‎<<_8, [8]>> details) port definition `SDD_Interface_PT`, which is used by the testcase and the SDD to communicate with each other.

== SCTP interface

To handle SCTP connections, SDD relies on the IPL4asp test port <<_5, [5]>>, thus the supported SCTP specifications also apply here. The user can control the SCTP association Id, stream Id and PPID. The testcase can also assign a specific local address for each SCTP association.

== Configuration

SDD can be configured using the TITAN RTE configuration file. The description of the specific parameters can be found in the SCTP Daemon Dynamic User’s Guide <<_4, [4]>>.

== Logging

The logging mechanism logs information either to the log file or output console. The user can set the amount and type of logging in the configuration file. These logging options are described in <<_6, [6]>>.

== Error Handling

Erroneous behaviour detected during runtime is directed into the RTE log file. The following two types of messages are taken care of:

* Errors: information about errors detected is provided. If an error occurs the execution of the test case will stop immediately. The test ports will be unmapped.
* Warnings: information about warnings detected is provided. The execution continues after the warning is shown.

== Limitations

* To handle SCTP connections, SDD relies on the IPL4asp test port. Therefore, the limitations described in <<_5, [5]>> also apply here.
* Several testcases can subscribe to the same association. If the association is closed by one of the testcases, the others are not notified about it.
