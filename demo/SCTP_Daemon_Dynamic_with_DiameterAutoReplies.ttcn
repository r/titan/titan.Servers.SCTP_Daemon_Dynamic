///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:           SCTP_Daemon_Dynamic_with_DiameterAutoReplies.ttcn
//  Description:    SDD behaviour with Diameter Auto Replies
//  Rev:            <RnXnn>
//  Prodnr:         CNL 113 630
//  Updated:        2010-11-24
//  Contact:        http://ttcn.ericsson.se

module SCTP_Daemon_Dynamic_with_DiameterAutoReplies
{

//=========================================================================
// Import Part
//=========================================================================

import from SCTP_Daemon_Dynamic all;
import from SCTP_Daemon_Dynamic_Types all;
import from SCTP_Daemon_Dynamic_Interface_Definitions all;

import from IPL4asp_Types all;

type record of boolean BooleanList;

//=========================================================================
//Component Types
//=========================================================================

//type component DIAMETER_SDD_CT extends SDD_CT {
//  var BooleanList vc_userStates := {};
//}

//=========================================================================
// Functions
//=========================================================================

function f_HandleDeviceWatchdog(in integer associationIndex, in ASP_RecvFrom sctpReceived, out ASP_Send sctpReply)
runs on SDD_CT
return boolean {
  if(lengthof(sctpReceived.msg) > 6) {
    // Device Watchdog: bytes #4-6 of the Diameter message
    if(sctpReceived.msg[4] == '00'O and sctpReceived.msg[5] == '01'O and sctpReceived.msg[6] == '18'O) {
      // bit #1 of the 3rd byte indicates whether the message is reply(1) or request(0)
      var bitstring thirdByte := oct2bit(sctpReceived.msg[3]);
      // we only send reply to the incoming request
      if(thirdByte[0] == '0'B) {
        thirdByte[0] := '1'B;
        
        sctpReply.connId := sctpReceived.connId;
        sctpReply.proto := sctpReceived.proto;
        sctpReply.msg := sctpReceived.msg;
        sctpReply.msg[3] := bit2oct(thirdByte);
        return true;
      }
    }
  }
  
  return false;
}

function f_SendAutoSetup(in integer associationIndex, in SAC_STATE newState, out ASP_Send sctpMsg)
runs on SDD_CT 
return boolean {
  if(newState == SCTP_COMM_UP) {
    if(vc_associationRuntimeAttributesList[associationIndex].userState == "") {
      sctpMsg := {
        vc_associationRuntimeAttributesList[associationIndex].socket,
        {sctp:={0,0,omit,omit}},
        'AABBCCDDEEFF'O
      }
      return true;
    }
  } else if(vc_associationRuntimeAttributesList[associationIndex].userState == "INITIALIZED") {
    // do something different
  } else {
    log("ERROR: unhandled user state ", vc_associationRuntimeAttributesList[associationIndex].userState);
  }
  
  
  return false;
}

//=========================================================================
// Testcases
//=========================================================================

testcase tc_SDD_with_DiameterAutoReplies() runs on SDD_CT {
  
  // register my Device Watchdog detector & answerer
  f_RegisterAutoReplyFunction(refers(f_HandleDeviceWatchdog));
  
  // register my auto setup sending function
  f_RegisterAutoStateChangeHandlerFunction(refers(f_SendAutoSetup));
  
  // run the SCTP Daemon's main function
  f_SDD();
  
}

//=========================================================================
// Control
//=========================================================================

control {
  execute(tc_SDD_with_DiameterAutoReplies());
}

}  // end of module
